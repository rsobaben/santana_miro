<?php
class InstallShell extends AppShell {
    public function main() {
    	$this->hr();
        $this->out('Creation of the necessary tables');
        $this->hr();
        if (!config('database')) {
			$this->out(__d('cake_console', 'Your database configuration was not found. Take a moment to create one.'), true);
			return $this->DbConfig->execute();
		}else{
			$this->dispatchShell('schema create');
			return true;
		}
    }
}