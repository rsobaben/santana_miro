<div class="panel panel-default">	
	<div class="panel-heading">
        <?php printf("<?php echo __('%s %s'); ?>", strpos($action, 'add') === false ? 'Edit' : 'Add', $singularHumanName); ?> 
        <div class="pull-right text-right"> 
<?php
            echo "\t\t\t<?php echo \$this->Html->link(__('%s List', '<i class=\"glyphicon glyphicon-th-list\"></i>'), array('action' => 'find'), array('escape' => false, 'class' => 'btn btn-xs btn-default')); ?>\n";
            if (strpos($action, 'edit') !== false) : 
                echo "\t\t\t<?php echo \$this->Form->postLink(__('%s Delete', '<i class=\"glyphicon glyphicon-trash\"></i>'), array('action' => 'delete', \$this->request->data('{$modelClass}.{$primaryKey}')), array('escape' => false, 'class' => 'btn btn-xs btn-danger'), __('Are you sure you want to delete # %s?', \$this->request->data('{$modelClass}.{$primaryKey}'))); ?>\n";
                echo "\t\t\t<?php echo \$this->Html->link(__('%s Add', '<i class=\"glyphicon glyphicon-plus\"></i>'), array('action' => 'add'), array('escape' => false, 'class' => 'btn btn-xs btn-success')); ?>\n"; 
            endif;
           ?>
        </div>        
    </div>
<?php
    echo "\t<?php\n";
    echo "\t\techo \$this->Form->create('{$modelClass}',array(
        'inputDefaults' => array(
            'class' => 'form-control',
            'div'   => array(
                'class' => 'form-group'
            )
        )
    )); ?>\n"; ?>
    <div class="panel-body">
<?php
        echo "\t<?php\n";
		foreach ($fields as $field) :
			if (strpos($action, 'add') !== false && $field == $primaryKey) :
				continue;
			elseif (!in_array($field, array('created', 'modified', 'updated'))) :
                $extra = "";
                switch ($schema[$field]['type']) :
                    case 'integer':
                        if ($schema[$field]['length'] < 5) :
                            $extra = ", array('before' => '<label>', 'after' => '</label>', 'type' => 'checkbox', 'class' => false, 'div' => array('class' => 'checkbox'))";
                        endif;
                        break;
                    
                endswitch;
				echo "\t\techo \$this->Form->input('{$field}'{$extra});\n";
			endif;
		endforeach;
		if (!empty($associations['hasAndBelongsToMany'])) :
			foreach ($associations['hasAndBelongsToMany'] as $assocName => $assocData) :
				echo "\t\techo \$this->Form->input('{$assocName}');\n";
			endforeach;
		endif;
		echo "?>\n";
    ?>   
	</div>
    <div class="panel-footer">
<?php echo "\t\t<?php echo \$this->Form->submit(__('Send'), array('class' => 'btn btn-primary', 'div' => array('class' => 'text-right'))); ?>\n"; ?>
    </div>
    <?php echo "<?php echo \$this->Form->end(); ?>\n"; ?>
</div>

