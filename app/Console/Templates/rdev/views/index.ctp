<?php echo "<?php echo \$this->element('rdev/search'); ?>\n"; ?>
<div class="panel panel-default">	
	<div class="panel-heading">
        <?php echo "<?php echo __('{$pluralHumanName}'); ?>"; ?> 
        <div class="pull-right text-right">
<?php
            echo "\t\t\t<?php echo \$this->Html->link(__('%s List', '<i class=\"glyphicon glyphicon-th-list\"></i>'), array('action' => 'find'), array('escape' => false, 'class' => 'btn btn-xs btn-default')); ?>\n";
            echo "\t\t\t<?php echo \$this->Html->link(__('%s Add',  '<i class=\"glyphicon glyphicon-plus\"></i>'), array('action' => 'add'), array('escape' => false, 'class' => 'btn btn-xs btn-success')); ?>\n"; ?>
        </div>
    </div>
<?php echo "<?php if (!empty(\${$pluralVar})) : ?>\n"; ?>
	<table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
<?php 
			foreach ($fields as $field): 
				echo "\t\t\t<th><?php echo \$this->Paginator->sort('{$field}'); ?></th>\n";
			endforeach; ?>
            <th class="actions text-right"><?php echo "<?php echo __('Actions'); ?>"; ?></th>
        </tr>
	<?php
	echo "<?php foreach (\${$pluralVar} as \${$singularVar}): ?>\n";
	echo "\t<tr>\n";
		foreach ($fields as $field) {
			$isKey = false;
			if (!empty($associations['belongsTo'])) {
				foreach ($associations['belongsTo'] as $alias => $details) {
					if ($field === $details['foreignKey']) {
						$isKey = true;
						echo "\t\t<td>\n\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t</td>\n";
						break;
					}
				}
			}
			if ($isKey !== true) {
				echo "\t\t<td><?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;</td>\n";
			}
		}

		echo "\t\t<td class=\"col-md-3 text-right\">\n";
		echo "\t\t\t<?php echo \$this->Html->link(__('%s View', '<i class=\"glyphicon glyphicon-file\"></i>'), array('action' => 'view', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('escape' => false, 'class' => 'btn btn-xs btn-default')); ?>\n";
		echo "\t\t\t<?php echo \$this->Html->link(__('%s Edit', '<i class=\"glyphicon glyphicon-edit\"></i>'), array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('escape' => false, 'class' => 'btn btn-xs btn-warning')); ?>\n";
		echo "\t\t\t<?php echo \$this->Form->postLink(__('%s Delete', '<i class=\"glyphicon glyphicon-trash\"></i>'), array('action' => 'delete', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('escape' => false, 'class' => 'btn btn-xs btn-danger'), __('Are you sure you want to delete # %s?', \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>\n";
		echo "\t\t</td>\n";
	echo "\t</tr>\n";

	echo "<?php endforeach; ?>\n";
	?>
	</table>
<?php echo "<?php else : ?>"; ?> 
    <br>
    <div class="alert alert-block alert-warning">
        <?php echo "<?php echo __('Nothing found!'); ?>"; ?> 
    </div>
<?php echo "<?php endif; ?>"; ?> 
    <div class="panel-footer text-center">
        <?php echo "<?php echo \$this->element('rdev/paginate'); ?>&nbsp;"; ?> 
    </div>
</div>
