<div class="panel panel-default">	
	<div class="panel-heading">
        <?php printf("<?php echo __('View %s'); ?>", $singularHumanName); ?> 
        <div class="pull-right text-right">
<?php
            echo "\t\t\t<?php echo \$this->Html->link(__('%s List', '<i class=\"glyphicon glyphicon-th-list\"></i>'), array('action' => 'find'), array('escape' => false, 'class' => 'btn btn-xs btn-default')); ?>\n";
            echo "\t\t\t<?php echo \$this->Html->link(__('%s Edit', '<i class=\"glyphicon glyphicon-edit\"></i>'), array('action' => 'edit', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('escape' => false, 'class' => 'btn btn-xs btn-warning')); ?>\n";
            echo "\t\t\t<?php echo \$this->Form->postLink(__('%s Delete', '<i class=\"glyphicon glyphicon-trash\"></i>'), array('action' => 'delete', \${$singularVar}['{$modelClass}']['{$primaryKey}']), array('escape' => false, 'class' => 'btn btn-xs btn-danger'), __('Are you sure you want to delete # %s?', \${$singularVar}['{$modelClass}']['{$primaryKey}'])); ?>\n";
            echo "\t\t\t<?php echo \$this->Html->link(__('%s Add', '<i class=\"glyphicon glyphicon-plus\"></i>'), array('action' => 'add'), array('escape' => false, 'class' => 'btn btn-xs btn-success')); ?>\n"; ?>
        </div>
    </div>
    
    <table class="table table-striped" cellpadding="0" cellspacing="0"><?php
foreach ($fields as $field) {
	$isKey = false;
	if (!empty($associations['belongsTo'])) {
		foreach ($associations['belongsTo'] as $alias => $details) {
			if ($field === $details['foreignKey']) {
				$isKey = true;
				echo "\n\t<tr>\n\t\t<th scope=\"row\"><?php echo __('" . Inflector::humanize(Inflector::underscore($alias)) . "'); ?></th>\n";
				echo "\t\t<td>\n\t\t\t<?php echo \$this->Html->link(\${$singularVar}['{$alias}']['{$details['displayField']}'], array('controller' => '{$details['controller']}', 'action' => 'view', \${$singularVar}['{$alias}']['{$details['primaryKey']}'])); ?>\n\t\t\t&nbsp;\n\t\t</td>\n";
                echo "\t</tr>\n";
				break;
			}
		}
	}
	if ($isKey !== true) {
		echo "\n\t<tr>\n\t\t<th scope=\"row\"><?php echo __('" . Inflector::humanize($field) . "'); ?></th>\n";
		echo "\t\t<td><?php echo h(\${$singularVar}['{$modelClass}']['{$field}']); ?>&nbsp;</td>\n";
        echo "\t</tr>\n";
	}
}
?> 
    </table>
    <div class="panel-footer text-right">
<?php echo "\t\t<?php echo \$this->Html->link(__('Cancel'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?>\n"; ?>
    </div>
</div>
