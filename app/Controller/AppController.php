<?php
App::uses('Controller', 'Controller');

class AppController extends Controller
{
    public $components = array(
        'Session',
        'Auth' => array(
            'authenticate' => array(
                'Form' => array(
                    'userModel' => 'User',
                    'fields' => array(
                        'username' => 'email',
                        'password' => 'password',
                        'userScope'    => array('User.active' => true)
                    ),
                    'passwordHasher' => array(
                        'className' => 'Simple',
                        'hashType' => 'sha256'
                    ),
                )
            ),
           // 'authorize' => 'Controller',
            'loginAction' => array(
                'controller' => 'users',
                'action'     => 'login',
                'rdev'     => false,
                'plugin'     => null
            ),
            'authError' => 'Você tem que estar logado para ver este conteúdo =/ !',
            'loginRedirect'  => '/rdev',
        ),
        'Paginator'
    );
    public function isAuthorized ($user) {
        if ($this->isAdmin()) {
            return !empty($user);
        }  
        return true;
    }
	
    public function isAdmin() {
        return (isset($this->request->params['prefix']) && $this->request->params['prefix'] == 'rdev') ||
               (isset($this->request->params['rdev']) && $this->request->params['rdev']);
    }
	public function beforeFilter() {
        if ($this->isAdmin()) {
            $this->layout = 'rdev_default';
            $this->set('the_user', $this->Auth->user());
        } else {
            $this->Auth->allow();
            $this->loadModel('Setting');
            $this->set('site', $this->Setting->getConfig());
        }
    }
    public function canUploadMedias($ref, $ref_id) {
        return true;
    }
    public function rdev_find($rem = null){
        $model = is_array($this->uses) ? $this->uses[0] : Inflector::classify($this->name);
        if (isset($this->request->data[$model]['key'])) {
            $keys ='';
            foreach (explode(' ', $this->request->data[$model]['key']) as $key) {
                if (strlen($key) > 3) {
                    $keys .= " {$key}";
                }
            }
            $this->Session->write("{$model}.search.key", trim($keys));
        }
        if (!is_null($rem)) {
            $keys = explode(' ', $this->Session->read("{$model}.search.key"));
            $id = array_search(urldecode($rem), $keys);
            if ($id !== false) {
                unset($keys[$id]);
            }
            if (empty($keys)) {
               $this->Session->delete("{$model}.search.key");
            } else {
                $this->Session->write("{$model}.search.key", implode(' ', $keys));
            }
            return $this->redirect(array('action' => 'find'));
        }
        if ($_key = $this->Session->read("{$model}.search.key")) {
            $columns = $this->$model->schema();
            foreach ($columns as $key => $value) {
                if ($value['type'] != 'string' || in_array($key, array('senha','password', 'resenha'))) {
                    unset($columns[$key]);
                }
            }
            $_key = explode(' ', $_key);
            $opts = array();
            foreach (array_keys($columns) as $col) {
                foreach ($_key as $k) {
                    $opts[]["{$model}.{$col} LIKE "] = "%{$k}%";
                }
            }
            $this->Paginator->settings['conditions'][] = array('OR' => $opts);
            $this->request->data[$model]['key'] = implode(' ', $_key);
        } else { 
            $this->redirect(array('action' => 'index'));
        }
        
        $this->rdev_index();
        $this->render('rdev_index');
    }

    public function rdev_clear() {
        $model = is_array($this->uses) ? $this->uses[0] : Inflector::classify($this->name);
        $this->Session->delete("{$model}.search");
        $this->redirect(array('action' => 'index'));
    }
	public function sendMail($data, $to, $subject = 'Contato pelo Site', $template = null, $layout = 'default') {
		App::uses('CakeEmail', 'Network/Email');
        if (!is_array($to)) {
            $to = explode(';', $to);
        }
		$email = new CakeEmail('smtp');
        $email->subject($subject)
              ->to($to)
              ->emailFormat('html')
              ->viewVars(array('data' => $data))
              ->template($template, $layout);
        return $email->send();
	}
};