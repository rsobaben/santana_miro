<?php
App::uses('AppController', 'Controller');
class BannersController extends AppController {
	public $components = array('Paginator', 'Session');
	public $helpers = array('Media.Media');
/**
 * rdev_index method
 *
 * @return void
 */
	public function rdev_index() {
        //$this->Paginator->settings['conditions']['Banner.active']=0;
		$this->Banner->recursive = 0;
		$this->set('banners', $this->Paginator->paginate());
	}

/**
 * rdev_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_view($id = null) {
		if (!$this->Banner->exists($id)) {
			throw new NotFoundException(__('Invalid banner'));
		}
		$options = array(
            'conditions' => array('Banner.' . $this->Banner->primaryKey => $id)
        );
		$this->set('banner', $this->Banner->find('first', $options));
	}

/**
 * rdev_add method
 *
 * @return void
 */
	public function rdev_add() {
		if ($this->request->is('post')) {
			$this->Banner->create();
			if ($this->Banner->save($this->request->data)) {
				$this->Session->setFlash(__('The banner has been saved.'), 'rdev/flash', array('class' => 'success'));
				return $this->redirect(array('action' => 'edit', $this->Banner->id));
			} else {
				$this->Session->setFlash(__('The banner could not be saved. Please, try again.'), 'rdev/flash', array('class' => 'danger'));
			}
		}
	}

/**
 * rdev_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_edit($id = null) {
		if (!$this->Banner->exists($id)) {
			throw new NotFoundException(__('Invalid banner'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Banner->save($this->request->data)) {
				$this->Session->setFlash(__('The banner has been updated.'), 'rdev/flash', array('class' => 'success'));
				return $this->redirect(array('action' => 'edit', $id));
			} else {
				$this->Session->setFlash(__('The banner could not be updated. Please, try again.'), 'rdev/flash', array('class' => 'danger'));
			}
		} else {
			$options = array('conditions' => array('Banner.' . $this->Banner->primaryKey => $id));
			$this->request->data = $this->Banner->find('first', $options);
		}
	}

/**
 * rdev_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_delete($id = null) {
		$this->Banner->id = $id;
		if (!$this->Banner->exists()) {
			throw new NotFoundException(__('Invalid banner'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Banner->delete()) {
			$this->Session->setFlash(__('The banner has been deleted.'), 'rdev/flash', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The banner could not be deleted. Please, try again.'), 'rdev/flash', array('class' => 'danger'));
		}
		return $this->redirect(array('action' => 'index'));
	} 
}
