<?php
App::uses('AppController', 'Controller');
class BrandsController extends AppController {
	public $components = array('Paginator', 'Session');

/**
 * rdev_index method
 *
 * @return void
 */
	public function rdev_index() {
        //$this->Paginator->settings['conditions']['Brand.active']=0;
		$this->Brand->recursive = 0;
		$this->set('brands', $this->Paginator->paginate());
	}

/**
 * rdev_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_view($id = null) {
		if (!$this->Brand->exists($id)) {
			throw new NotFoundException(__('Invalid brand'));
		}
		$options = array(
            'conditions' => array('Brand.' . $this->Brand->primaryKey => $id)
        );
		$this->set('brand', $this->Brand->find('first', $options));
	}

/**
 * rdev_add method
 *
 * @return void
 */
	public function rdev_add() {
		if ($this->request->is('post')) {
			$this->Brand->create();
			if ($this->Brand->save($this->request->data)) {
				$this->Session->setFlash(__('The brand has been saved.'), 'rdev/flash', array('class' => 'success'));
				return $this->redirect(array('action' => 'edit', $this->Brand->id));
			} else {
				$this->Session->setFlash(__('The brand could not be saved. Please, try again.'), 'rdev/flash', array('class' => 'danger'));
			}
		}
	}

/**
 * rdev_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_edit($id = null) {
		if (!$this->Brand->exists($id)) {
			throw new NotFoundException(__('Invalid brand'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Brand->save($this->request->data)) {
				$this->Session->setFlash(__('The brand has been updated.'), 'rdev/flash', array('class' => 'success'));
				return $this->redirect(array('action' => 'edit', $id));
			} else {
				$this->Session->setFlash(__('The brand could not be updated. Please, try again.'), 'rdev/flash', array('class' => 'danger'));
			}
		} else {
			$options = array('conditions' => array('Brand.' . $this->Brand->primaryKey => $id));
			$this->request->data = $this->Brand->find('first', $options);
		}
	}

/**
 * rdev_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_delete($id = null) {
		$this->Brand->id = $id;
		if (!$this->Brand->exists()) {
			throw new NotFoundException(__('Invalid brand'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Brand->delete()) {
			$this->Session->setFlash(__('The brand has been deleted.'), 'rdev/flash', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The brand could not be deleted. Please, try again.'), 'rdev/flash', array('class' => 'danger'));
		}
		return $this->redirect(array('action' => 'index'));
	} 
}
