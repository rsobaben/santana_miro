<?php
App::uses('AppController', 'Controller');
class CategoriesController extends AppController {
	public $components = array('Paginator', 'Session');

	public function menu() {
		$categories = $this->Category->find('all', array('conditions' => array('Category.active'), 'order' => array('Category.order' => 'ASC')));
		
		$this->layout = 'ajax';
		$this->set(compact('categories'));
	}
	
########################################### rdev #############################

/**
 * rdev_index method
 *
 * @return void
 */
	public function rdev_index() {
        //$this->Paginator->settings['conditions']['Category.active']=0;
		$this->Category->recursive = 0;
		$this->set('categories', $this->Paginator->paginate());
	}

/**
 * rdev_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_view($id = null) {
		if (!$this->Category->exists($id)) {
			throw new NotFoundException(__('Invalid category'));
		}
		$options = array(
            'conditions' => array('Category.' . $this->Category->primaryKey => $id)
        );
		$this->set('category', $this->Category->find('first', $options));
	}

/**
 * rdev_add method
 *
 * @return void
 */
	public function rdev_add() {
		if ($this->request->is('post')) {
			$this->Category->create();
			if ($this->Category->save($this->request->data)) {
				$this->Session->setFlash(__('The category has been saved.'), 'rdev/flash', array('class' => 'success'));
				return $this->redirect(array('action' => 'edit', $this->Category->id));
			} else {
				$this->Session->setFlash(__('The category could not be saved. Please, try again.'), 'rdev/flash', array('class' => 'danger'));
			}
		}
	}

/**
 * rdev_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_edit($id = null) {
		if (!$this->Category->exists($id)) {
			throw new NotFoundException(__('Invalid category'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Category->save($this->request->data)) {
				$this->Session->setFlash(__('The category has been updated.'), 'rdev/flash', array('class' => 'success'));
				return $this->redirect(array('action' => 'edit', $id));
			} else {
				$this->Session->setFlash(__('The category could not be updated. Please, try again.'), 'rdev/flash', array('class' => 'danger'));
			}
		} else {
			$options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
			$this->request->data = $this->Category->find('first', $options);
		}
	}

/**
 * rdev_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_delete($id = null) {
		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			throw new NotFoundException(__('Invalid category'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Category->delete()) {
			$this->Session->setFlash(__('The category has been deleted.'), 'rdev/flash', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The category could not be deleted. Please, try again.'), 'rdev/flash', array('class' => 'danger'));
		}
		return $this->redirect(array('action' => 'index'));
	} 
}
