<?php
App::uses('Component', 'Controller');

class SslComponent extends Component {
	public $components = array('RequestHandler');
	public $Controller = null;
	public $__isLocal = false; 

	public function initialize(Controller  $Controller) {
		$this->Controller = $Controller;
		$this->__isLocal =  env('SERVER_NAME') == 'localhost' ||
							env('SERVER_NAME') == '127.0.0.1';
	}
	public function force() {
		if(!$this->RequestHandler->isSSL()) {
			return $this->__isLocal ? true : $this->Controller->redirect('https://'.$this->__url(443));
		}
	}
	public function unforce() {
		if($this->RequestHandler->isSSL()) {
			return $this->__isLocal ? true : $this->Controller->redirect('http://' . $this->__url());
		}
	}
	public function __url($default_port = 80) {
		$port = env('SERVER_PORT') == $default_port ? '' : ':' . env('SERVER_PORT');
		return env('SERVER_NAME') . env('REQUEST_URI');
	}

	public function startUp(Controller $controller){}
	public function beforeRender(Controller $controller){}
	public function shutDown(Controller $controller){}
}; 