<?php
App::uses('AppController', 'Controller');
class PagesController extends AppController {
	public $components = array('Paginator', 'Session');
	public $helpers = array('Media.Media');

	public function contact() {
		$this->loadModel('Contact');
		$page = $this->Page->find('first', array('conditions' => array('Page.id' => 2)));
		
		if ($this->request->is(array('post', 'put'))) {
			$this->Contact->set($this->request->data);
			if ($this->Contact->validates()) {
				parent::sendMail($this->request->data, $this->viewVars['site']['Site']['email'], 'Contato pelo site', 'contact');
				$this->Session->setFlash(__('Message Sent successfully'), 'rdev/flash', array('class' => 'success'));
				return $this->redirect($this->referer());
			}
			$this->Session->setFlash(__('Error while sending message'), 'rdev/flash', array('class' => 'danger'));
		}
		
		$this->set(compact('page'));
	}
	public function about() {
		$page = $this->Page->find('first', array('conditions' => array('Page.id' => 1)));
		
		$this->set(compact('page'));
	}
########################################### rdev #############################

	
/**
 * rdev_index method
 *
 * @return void
 */
	public function rdev_index() {
        //$this->Paginator->settings['conditions']['Page.active']=0;
		$this->Page->recursive = 0;
		$this->set('pages', $this->Paginator->paginate());
	}

/**
 * rdev_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_view($id = null) {
		if (!$this->Page->exists($id)) {
			throw new NotFoundException(__('Invalid page'));
		}
		$options = array(
            'conditions' => array('Page.' . $this->Page->primaryKey => $id)
        );
		$this->set('page', $this->Page->find('first', $options));
	}

/**
 * rdev_add method
 *
 * @return void
 */
	public function rdev_add() {
		if ($this->request->is('post')) {
			$this->Page->create();
			if ($this->Page->save($this->request->data)) {
				$this->Session->setFlash(__('The page has been saved.'), 'rdev/flash', array('class' => 'success'));
				return $this->redirect(array('action' => 'edit', $this->Page->id));
			} else {
				$this->Session->setFlash(__('The page could not be saved. Please, try again.'), 'rdev/flash', array('class' => 'danger'));
			}
		}
	}

/**
 * rdev_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_edit($id = null) {
		if (!$this->Page->exists($id)) {
			throw new NotFoundException(__('Invalid page'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Page->save($this->request->data)) {
				$this->Session->setFlash(__('The page has been updated.'), 'rdev/flash', array('class' => 'success'));
				return $this->redirect(array('action' => 'edit', $id));
			} else {
				$this->Session->setFlash(__('The page could not be updated. Please, try again.'), 'rdev/flash', array('class' => 'danger'));
			}
		} else {
			$options = array('conditions' => array('Page.' . $this->Page->primaryKey => $id));
			$this->request->data = $this->Page->find('first', $options);
		}
	}

/**
 * rdev_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_delete($id = null) {
		$this->Page->id = $id;
		if (!$this->Page->exists()) {
			throw new NotFoundException(__('Invalid page'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Page->delete()) {
			$this->Session->setFlash(__('The page has been deleted.'), 'rdev/flash', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The page could not be deleted. Please, try again.'), 'rdev/flash', array('class' => 'danger'));
		}
		return $this->redirect(array('action' => 'index'));
	} 

}
