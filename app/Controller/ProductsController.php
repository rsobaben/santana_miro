<?php
App::uses('AppController', 'Controller');
class ProductsController extends AppController {
	public $components = array('Paginator', 'Session');
    public $helpers = array('Media.Media');
	
	public function index($category = null) {
		$conditions = array(
			'Product.active' => 1
		);
		$title = 'Produtos';
		
		if (is_numeric($category)) {
			$conditions['Product.category_id'] = $category;
			
			$_category = $this->Product->Category->findById($category);
			if ($_category) {
				$_category['Category']['name'] =  str_replace('<br>', '', $_category['Category']['name']);
				$title = "{$_category['Category']['name']} > {$title}";
			}
		}
		if ($this->request->is(array('post', 'put'))) {
			if (($key = $this->request->data('Product.key'))) {
				$conditions['Product.name LIKE'] = "%{$key}%";
			}
		}
		if (isset($this->request->params['named']) && isset($this->request->params['named']['brand'])) {
			$conditions['Product.brand_id'] = (int) $this->request->params['named']['brand'];
			
			$_brand = $this->Product->Brand->findById((int) $this->request->params['named']['brand']);
			if ($_brand) {
				$_brand['Brand']['name'] =  str_replace('<br>', '', $_brand['Brand']['name']);
				$title = "{$_brand['Brand']['name']} > {$title}";
			}
		}
		$this->Paginator->settings = array(
			'Product' => array(
				'conditions' => $conditions,
				'limit'      => 10,
				'order'      => array('Product.updated' => 'desc')
			)
		);
		
		$itens = $this->Paginator->paginate('Product');
		
		$this->set(compact('title', 'itens'));
	}

	public function view($id = null) {
		if (!$this->Product->exists($id)) {
			throw new NotFoundException(__('Invalid product'));
		}
		$options = array(
            'conditions' => array('Product.id' => $id)
        );
		$this->set('product', $this->Product->find('first', $options));
	}
########################################### rdev #############################

/**
 * rdev_index method
 *
 * @return void
 */
	public function rdev_index() {
        //$this->Paginator->settings['conditions']['Product.active']=0;
		$this->Product->recursive = 0;
		$this->set('products', $this->Paginator->paginate());
	}

/**
 * rdev_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_view($id = null) {
		if (!$this->Product->exists($id)) {
			throw new NotFoundException(__('Invalid product'));
		}
		$options = array(
            'conditions' => array('Product.' . $this->Product->primaryKey => $id)
        );
		$this->set('product', $this->Product->find('first', $options));
	}

/**
 * rdev_add method
 *
 * @return void
 */
	public function rdev_add() {
		if ($this->request->is('post')) {
			$this->Product->create();
			if ($this->Product->save($this->request->data)) {
				$this->Session->setFlash(__('The product has been saved.'), 'rdev/flash', array('class' => 'success'));
				return $this->redirect(array('action' => 'edit', $this->Product->id));
			} else {
				$this->Session->setFlash(__('The product could not be saved. Please, try again.'), 'rdev/flash', array('class' => 'danger'));
			}
		}
		$brands = $this->Product->Brand->find('list');
		$categories = $this->Product->Category->find('list');
		$this->set(compact('brands', 'categories'));
	}

/**
 * rdev_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_edit($id = null) {
		if (!$this->Product->exists($id)) {
			throw new NotFoundException(__('Invalid product'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Product->save($this->request->data)) {
				$this->Session->setFlash(__('The product has been updated.'), 'rdev/flash', array('class' => 'success'));
				return $this->redirect(array('action' => 'edit', $id));
			} else {
				$this->Session->setFlash(__('The product could not be updated. Please, try again.'), 'rdev/flash', array('class' => 'danger'));
			}
		} else {
			$options = array('conditions' => array('Product.' . $this->Product->primaryKey => $id));
			$this->request->data = $this->Product->find('first', $options);
		}
		$brands = $this->Product->Brand->find('list');
		$categories = $this->Product->Category->find('list');
		$this->set(compact('brands', 'categories'));
	}

/**
 * rdev_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_delete($id = null) {
		$this->Product->id = $id;
		if (!$this->Product->exists()) {
			throw new NotFoundException(__('Invalid product'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Product->delete()) {
			$this->Session->setFlash(__('The product has been deleted.'), 'rdev/flash', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The product could not be deleted. Please, try again.'), 'rdev/flash', array('class' => 'danger'));
		}
		return $this->redirect(array('action' => 'index'));
	} 
}
