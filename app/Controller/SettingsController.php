<?php
App::uses('AppController', 'Controller');
class SettingsController extends AppController {
	public $components = array('Paginator', 'Session');

/**
 * rdev_index method
 *
 * @return void
 */
	public function rdev_index() {
        $this->Paginator->settings['order']['Setting.setting']='asc';
		$this->Setting->recursive = 0;
		$this->set('Settings', $this->Paginator->paginate());
	}

/**
 * rdev_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_view($id = null) {
		if (!$this->Setting->exists($id)) {
			throw new NotFoundException(__('Invalid setting'));
		}
		$options = array(
            'conditions' => array('Setting.' . $this->Setting->primaryKey => $id)
        );
		$this->set('setting', $this->Setting->find('first', $options));
	}

/**
 * rdev_add method
 *
 * @return void
 */
	public function rdev_add() {
		if ($this->request->is('post')) {
			$this->Setting->create();
			if ($this->Setting->save($this->request->data)) {
				$this->Session->setFlash(__('The setting has been saved.'), 'rdev/flash', array('class' => 'success'));
				return $this->redirect(array('action' => 'edit', $this->Setting->id));
			} else {
				$this->Session->setFlash(__('The setting could not be saved. Please, try again.'), 'rdev/flash', array('class' => 'danger'));
			}
		}
	}

/**
 * rdev_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_edit($id = null) {
		if (!$this->Setting->exists($id)) {
			throw new NotFoundException(__('Invalid setting'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Setting->save($this->request->data)) {
				$this->Session->setFlash(__('The setting has been updated.'), 'rdev/flash', array('class' => 'success'));
				return $this->redirect(array('action' => 'edit', $id));
			} else {
				$this->Session->setFlash(__('The setting could not be updated. Please, try again.'), 'rdev/flash', array('class' => 'danger'));
			}
		} else {
			$options = array('conditions' => array('Setting.' . $this->Setting->primaryKey => $id));
			$this->request->data = $this->Setting->find('first', $options);
		}
	}

/**
 * rdev_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_delete($id = null) {
		$this->Setting->id = $id;
		if (!$this->Setting->exists()) {
			throw new NotFoundException(__('Invalid setting'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Setting->delete()) {
			$this->Session->setFlash(__('The setting has been deleted.'), 'rdev/flash', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('The setting could not be deleted. Please, try again.'), 'rdev/flash', array('class' => 'danger'));
		}
		return $this->redirect(array('action' => 'index'));
	} 
}
