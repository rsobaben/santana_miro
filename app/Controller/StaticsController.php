<?php
App::uses('AppController', 'Controller');

class StaticsController extends AppController {
	public $uses = array();
	public $helpers = array("Media.Media");

	public function rdev_index() {
	}

    public function home(){
        $this->loadModel('Product');
		
		$products = $this->Product->find('all', array(
			'conditions' => array(
				'Product.active',
				'Product.featured'
			),
			'limit' => 7
		));
		
		$this->loadModel('Banner');
		$banners = $this->Banner->find('all', array(
			'conditions' => array(
				'Banner.active' => 1
			),
			'order' => 'rand()'
		));
		$this->set(compact('products', 'banners'));
    }
}