<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');
/**
 * login method 
 *
 * @return void
 */ 
    public function login() {
        $this->layout = 'rdev_default';
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Session->setFlash(__('Combinação e-mail/senha inválida'), 'rdev/flash', array('class' => 'danger'));
            }
        }
    }
/**
 * logout method 
 *
 * @return void 
 */
    public function logout() {
        return $this->redirect($this->Auth->logout());
    }
    public function rdev_logout() {
        return $this->logout();
    }    
    public function recover() {
        $this->layout = 'rdev_default';
        if ($this->request->is(array('post', 'put'))) {
            $user = $this->User->findByEmail($this->request->data('User.email'));
            if ($user) {
                $token = sprintf('%d.%s.%s', $user['User']['id'], rand(10000,99999), md5($user['User']['email']));
                $this->User->id = $user['User']['id'];
                $this->User->saveField('token', $token);
                
            } else {
                $this->Session->setFlash(__('E-mail não encontrado!'), 'rdev/flash', array('class' => 'danger'));
            }
        }
    }
    public function redefine($token = null) {
        $this->layout = 'rdev_default';
        $user = $this->User->findByToken($token);
        if ($user) {
            if ($this->request->is(array('post', 'put'))) {
                if ($this->User->save($this->request->data)) {
                    $this->User->id = $this->request->data('User.id');
                    $this->User->saveField('token', null);
                    $this->request->data = $this->User->read();
                    if ($this->Auth->login($this->request->data['User'])) {
                        $this->Session->setFlash(__('Password changed!'), 'rdev/flash', array('class' => 'success'));
                        return $this->redirect($this->Auth->redirectUrl());
                    }
                }
                $this->Session->setFlash(__('Requisição inválida!'), 'rdev/flash', array('class' => 'danger'));
                return $this->redirect(array('action' => 'login'));
            }
            unset($user['User']['password']);
            $this->request->data = $user;
        } else {
            $this->Session->setFlash(__('Requisição inválida!'), 'rdev/flash', array('class' => 'danger'));
            return $this->redirect(array('action' => 'login'));
        }
    }

/**
 * rdev_index method
 *
 * @return void
 */
	public function rdev_index() {
        $this->Paginator->settings['conditions']['User.id >']=1;
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}

/**
 * rdev_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Usuário inválido'));
		}
		$options = array(
            'conditions' => array('User.' . $this->User->primaryKey => $id)
        );
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * rdev_add method
 *
 * @return void
 */
	public function rdev_add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Registro salvo com sucesso'), 'rdev/flash', array('class' => 'success'));
				return $this->redirect(array('action' => 'edit', $this->User->id));
			} else {
				$this->Session->setFlash(__('Falha ao adicionar registro. Tente novamente!'), 'rdev/flash', array('class' => 'danger'));
			}
		}
	}

/**
 * rdev_alter method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_alter($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Usuário inválido'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Registro alterado com sucesso.'), 'rdev/flash', array('class' => 'success'));
				return $this->redirect(array('action' => 'alter', $id));
			} else {
				$this->Session->setFlash(__('Falha ao alterar registro. Tente novamente!'), 'rdev/flash', array('class' => 'danger'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}
/**
 * rdev_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Usuário inválido'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Registro atualizado com sucesso.'), 'rdev/flash', array('class' => 'success'));
				return $this->redirect(array('action' => 'edit', $id));
			} else {
				$this->Session->setFlash(__('Erro ao atualziar registro. Tente novamente.'), 'rdev/flash', array('class' => 'danger'));
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}

/**
 * rdev_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function rdev_delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Usuário inválido'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('Registro removido com sucesso.'), 'rdev/flash', array('class' => 'success'));
		} else {
			$this->Session->setFlash(__('Falha ao remover registro. Tente novamente.'), 'rdev/flash', array('class' => 'danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}

}
