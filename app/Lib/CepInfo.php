<?php 

class CepInfo {
	const BASE = 'http://cep.republicavirtual.com.br/web_cep.php';

	public static function read($cep) {
		$url = sprintf('%s?cep=%s&formato=json', self::BASE, (string) $cep);
		$tmp = self::get($url);
		return (json_decode($tmp, true));
	}

	protected static function get($url, $data = null) {
		$sessao_curl = curl_init();
		curl_setopt($sessao_curl, CURLOPT_URL, $url);
		curl_setopt($sessao_curl, CURLOPT_FAILONERROR,    true);
		curl_setopt($sessao_curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($sessao_curl, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($sessao_curl, CURLOPT_TIMEOUT,        40);
		curl_setopt($sessao_curl, CURLOPT_RETURNTRANSFER, true);

		if (!empty($data)) {
			curl_setopt($sessao_curl, CURLOPT_POST, true);
			curl_setopt($sessao_curl, CURLOPT_POSTFIELDS, $data);
		}
		$resultado = curl_exec($sessao_curl);
		curl_close($sessao_curl);
		return $resultado;
	}
};