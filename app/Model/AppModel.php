<?php
App::uses('Model', 'Model');

class AppModel extends Model {
    public $message = 'erro ao processar requisição';
	public function unbindModelAll ($diff = array(),$reset = false) {
       $associations = array(
               'hasOne' => array_keys($this->hasOne),
               'hasMany' => array_keys($this->hasMany),
               'belongsTo' => array_keys($this->belongsTo),
               'hasAndBelongsToMany' => array_keys($this->hasAndBelongsToMany)
       );
       
       foreach ($associations as $relation => $model) {
               if (isset($diff[$relation])) {
                       $model = array_diff($model,$diff[$relation]);
               }
               
               $this->unbindModel(array($relation => $model),$reset);
       }
   }
}
