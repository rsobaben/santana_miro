<?php

class DataBrBehavior extends ModelBehavior {
	public $settings = array();

	public function setUp(Model $Model, $settings = array())
    {
    	$fields = array();
    	foreach ((array)$Model->schema() as $field => $conf) {
            if (in_array($field, array('id'))) {
                continue;
            }
            if ($conf['type'] == 'date' || $conf['type'] == 'datetime') {
                $fields[$field] = $conf['type'];
            }
    	} 
        $this->settings[$Model->name] = $fields;
    }

    public function beforeSave(Model $Model, $options = array()) {
        if (empty($this->settings[$Model->name])) {
            return true;
        }
        foreach ($this->settings[$Model->name] as $field => $type) {
            if (!empty($Model->data[$Model->alias][$field])) {
                $Model->data[$Model->alias][$field] = $this->datefy($Model, $Model->data[$Model->alias][$field], '-', $type);
            }
        }
        return true;
    }

    public function afterFind(Model $Model, $results, $primary = false) {
        if (empty($results) || empty($this->settings[$Model->name])) {
            return $results;
        }
        foreach ($this->settings[$Model->name] as $field => $type) {
            foreach ($results as $key => &$value) {
                if (isset($value[$Model->alias][$field]) && !empty($value[$Model->alias][$field])) {
                    $value[$Model->alias][$field] = $this->datefy($Model, $value[$Model->alias][$field], '/', $type, false, true);
                }
            }
        }
        return $results;
    }

    public function datefy($Model, $_input, $separator = '-', $type = 'date', $use_23_59_59 = false, $clean_if_zero = false) {
        
        if (empty($_input) || (($_input == '0000-00-00' || $_input == '0000-00-00 00:00:00')&& $clean_if_zero)) {
            return null;
        }
        $part = explode(' ', $_input);
        if (count($part) <2) {
            $part[] = ($type == 'datetime' ? ($use_23_59_59 ? '23:59:59': '00:00:00'): '');
        }
        $part[0] = explode('-', preg_replace('/\D/', '-', $part[0]));
        $part[0] = array_reverse($part[0]);
        return trim(implode($separator, $part[0]) . ($type == 'datetime' ? " {$part[1]}" : ''));
    }
}