<?php
App::uses('AppModel', 'Model');

class Contact extends AppModel {
	public $useTable = false;
    
	public $validate = array(
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
			),
		),
		'message' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		),
	);
}
