<?php
App::uses('AppModel', 'Model');
class Page extends AppModel {
	public $actsAs = array('Media.Media');
	public $displayField = 'title';

	public $validate = array(
		'title' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		)
	);
}
