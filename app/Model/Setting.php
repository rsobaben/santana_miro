<?php
App::uses('AppModel', 'Model');
/**
 * Setting Model
 *
 */
class Setting extends AppModel {

	public $displayField = 'label';
	public $validate = array(
		'label' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		),
		'field' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		),
	);
	
	public function makeReturn($data) {
		$return = array();
		foreach ($data as $d) {
			$return[$d['Setting']['setting']][$d['Setting']['field']] = $d['Setting']['value'];
		}
		return $return;
	}

	public function getConfig()
	{
		$data = $this->find('all', array('order' => array('Setting.setting'=> 'asc')));
		return $this->makeReturn($data);
	}

	public function getGroup($setting) {
		$data = $this->find('all', array('conditions' => array('Setting.setting' => $setting)));
		return $this->makeReturn($data);
	}

	public function getValue($key) {
		$parts = explode('.', $key);
		$data = $this->find('all', array('conditions' => array('Setting.setting' => $parts[0], 'Setting.field' => $parts[1])));	
		return $this->makeReturn($data);
	}
}