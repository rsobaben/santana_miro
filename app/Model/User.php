<?php
App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
/**
 * User Model
 *
 */
class User extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
			),
            'isUnique' => array(
                'rule'    => array('isUnique'),
            )
		),
		'password' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
			),
		),
	);
    
    public function beforeSave($options = array()) {
        if (isset($this->data['User']['confirm'])) {
            if ($this->data['User']['confirm'] != @$this->data['User']['password']) {
                $this->invalidate('password', __('Password and confirmation must match'));
                return false;
            }
            unset($this->data['User']['confirm']);
        }
        if (isset($this->data['User']['password']) && !empty($this->data['User']['password'])) {
            $passwordHasher = new SimplePasswordHasher(array('hashType' => 'sha256'));
            $this->data['User']['password'] = $passwordHasher->hash($this->data['User']['password']);
        }
        return true;
    }
}