<div class="panel panel-default">	
	<div class="panel-heading">
        <?php echo __('Edit Banner'); ?> 
        <div class="pull-right text-right"> 
			<?php echo $this->Html->link(__('%s List', '<i class="glyphicon glyphicon-th-list"></i>'), array('action' => 'find'), array('escape' => false, 'class' => 'btn btn-xs btn-default')); ?>
			<?php echo $this->Form->postLink(__('%s Delete', '<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $this->request->data('Banner.id')), array('escape' => false, 'class' => 'btn btn-xs btn-danger'), __('Are you sure you want to delete # %s?', $this->request->data('Banner.id'))); ?>
			<?php echo $this->Html->link(__('%s Add', '<i class="glyphicon glyphicon-plus"></i>'), array('action' => 'add'), array('escape' => false, 'class' => 'btn btn-xs btn-success')); ?>
        </div>        
    </div>
	<?php
		echo $this->Form->create('Banner',array(
        'inputDefaults' => array(
            'class' => 'form-control',
            'div'   => array(
                'class' => 'form-group'
            )
        )
    )); ?>
    <div class="panel-body">
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('url', array('after' => '<small>Ex: http://www.google.com</small>'));
		echo $this->Form->input('active', array('before' => '<label>', 'after' => '</label>', 'type' => 'checkbox', 'class' => false, 'div' => array('class' => 'checkbox')));
		
		echo $this->Media->iframe('Banner', $this->request->data('Banner.id'));
?>
   
	</div>
    <div class="panel-footer">
		<?php echo $this->Form->submit(__('Send'), array('class' => 'btn btn-primary', 'div' => array('class' => 'text-right'))); ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

