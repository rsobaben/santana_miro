<?php echo $this->element('rdev/search'); ?>
<div class="panel panel-default">	
	<div class="panel-heading">
        <?php echo __('Banners'); ?> 
        <div class="pull-right text-right">
			<?php echo $this->Html->link(__('%s List', '<i class="glyphicon glyphicon-th-list"></i>'), array('action' => 'find'), array('escape' => false, 'class' => 'btn btn-xs btn-default')); ?>
			<?php echo $this->Html->link(__('%s Add',  '<i class="glyphicon glyphicon-plus"></i>'), array('action' => 'add'), array('escape' => false, 'class' => 'btn btn-xs btn-success')); ?>
        </div>
    </div>
<?php if (!empty($banners)) : ?>
	<table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('url'); ?></th>
			<th><?php echo $this->Paginator->sort('active'); ?></th>
            <th class="actions text-right"><?php echo __('Actions'); ?></th>
        </tr>
	<?php foreach ($banners as $banner): ?>
	<tr>
		<td><?php echo h($banner['Banner']['id']); ?>&nbsp;</td>
		<td><?php echo h($banner['Banner']['name']); ?>&nbsp;</td>
		<td><?php echo h($banner['Banner']['url']); ?>&nbsp;</td>
		<td><i class="glyphicon glyphicon-<?php echo $banner['Banner']['active'] ? 'ok':'remove'; ?>"></i>&nbsp;</td>
		<td class="col-md-3 text-right">
			<?php echo $this->Html->link(__('%s View', '<i class="glyphicon glyphicon-file"></i>'), array('action' => 'view', $banner['Banner']['id']), array('escape' => false, 'class' => 'btn btn-xs btn-default')); ?>
			<?php echo $this->Html->link(__('%s Edit', '<i class="glyphicon glyphicon-edit"></i>'), array('action' => 'edit', $banner['Banner']['id']), array('escape' => false, 'class' => 'btn btn-xs btn-warning')); ?>
			<?php echo $this->Form->postLink(__('%s Delete', '<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $banner['Banner']['id']), array('escape' => false, 'class' => 'btn btn-xs btn-danger'), __('Are you sure you want to delete # %s?', $banner['Banner']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
<?php else : ?> 
    <br>
    <div class="alert alert-block alert-warning">
        <?php echo __('Nothing found!'); ?> 
    </div>
<?php endif; ?> 
    <div class="panel-footer text-center">
        <?php echo $this->element('rdev/paginate'); ?>&nbsp; 
    </div>
</div>
