<div class="panel panel-default">	
	<div class="panel-heading">
        <?php echo __('View Banner'); ?> 
        <div class="pull-right text-right">
			<?php echo $this->Html->link(__('%s List', '<i class="glyphicon glyphicon-th-list"></i>'), array('action' => 'find'), array('escape' => false, 'class' => 'btn btn-xs btn-default')); ?>
			<?php echo $this->Html->link(__('%s Edit', '<i class="glyphicon glyphicon-edit"></i>'), array('action' => 'edit', $banner['Banner']['id']), array('escape' => false, 'class' => 'btn btn-xs btn-warning')); ?>
			<?php echo $this->Form->postLink(__('%s Delete', '<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $banner['Banner']['id']), array('escape' => false, 'class' => 'btn btn-xs btn-danger'), __('Are you sure you want to delete # %s?', $banner['Banner']['id'])); ?>
			<?php echo $this->Html->link(__('%s Add', '<i class="glyphicon glyphicon-plus"></i>'), array('action' => 'add'), array('escape' => false, 'class' => 'btn btn-xs btn-success')); ?>
        </div>
    </div>
    
    <table class="table table-striped" cellpadding="0" cellspacing="0">
	<tr>
		<th scope="row"><?php echo __('Id'); ?></th>
		<td><?php echo h($banner['Banner']['id']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Created'); ?></th>
		<td><?php echo h($banner['Banner']['created']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Modified'); ?></th>
		<td><?php echo h($banner['Banner']['modified']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Name'); ?></th>
		<td><?php echo h($banner['Banner']['name']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Url'); ?></th>
		<td><?php echo h($banner['Banner']['url']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Active'); ?></th>
		<td><i class="glyphicon glyphicon-<?php echo $banner['Banner']['active'] ? 'ok':'remove'; ?>"></i>&nbsp;</td>
	</tr>
 
    </table>
    <div class="panel-footer text-right">
		<?php echo $this->Html->link(__('Cancel'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?>
    </div>
</div>
