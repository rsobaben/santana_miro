<div class="panel panel-default">	
	<div class="panel-heading">
        <?php echo __('View Brand'); ?> 
        <div class="pull-right text-right">
			<?php echo $this->Html->link(__('%s List', '<i class="glyphicon glyphicon-th-list"></i>'), array('action' => 'find'), array('escape' => false, 'class' => 'btn btn-xs btn-default')); ?>
			<?php echo $this->Html->link(__('%s Edit', '<i class="glyphicon glyphicon-edit"></i>'), array('action' => 'edit', $brand['Brand']['id']), array('escape' => false, 'class' => 'btn btn-xs btn-warning')); ?>
			<?php echo $this->Form->postLink(__('%s Delete', '<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $brand['Brand']['id']), array('escape' => false, 'class' => 'btn btn-xs btn-danger'), __('Are you sure you want to delete # %s?', $brand['Brand']['id'])); ?>
			<?php echo $this->Html->link(__('%s Add', '<i class="glyphicon glyphicon-plus"></i>'), array('action' => 'add'), array('escape' => false, 'class' => 'btn btn-xs btn-success')); ?>
        </div>
    </div>
    
    <table class="table table-striped" cellpadding="0" cellspacing="0">
	<tr>
		<th scope="row"><?php echo __('Id'); ?></th>
		<td><?php echo h($brand['Brand']['id']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Created'); ?></th>
		<td><?php echo h($brand['Brand']['created']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Modified'); ?></th>
		<td><?php echo h($brand['Brand']['modified']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Name'); ?></th>
		<td><?php echo h($brand['Brand']['name']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Active'); ?></th>
		<td><?php echo h($brand['Brand']['active']); ?>&nbsp;</td>
	</tr>
 
    </table>
    <div class="panel-footer text-right">
		<?php echo $this->Html->link(__('Cancel'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?>
    </div>
</div>
