<nav role="navigation" class="categories-links">
	<ul class="nav nav-pills">
	<?php foreach ($categories as  $category) : ?>
		<li class="<?php echo strpos($category['Category']['name'], '<br>') !== false ? 'double' : 'single'; ?>-line-link">
		<?php 
			echo $this->Html->link(
				$category['Category']['name'],
				array(
					'controller' => 'products',
					'slug'       => Inflector::slug(str_replace('<br>', ' ', $category['Category']['name']), '-'),
					'category'   => $category['Category']['id'], 
				),
				array('escape' => false)
			); ?>
		</li>
	<?php endforeach; ?>
	</ul>
</nav>