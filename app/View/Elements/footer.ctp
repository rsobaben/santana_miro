<div class="footer-part">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<p>
					<?php echo $site['Site']['address']; ?><br>
					CEP: <?php echo $site['Site']['zipcode']; ?> | <?php echo $site['Site']['city_state']; ?>
				</p>
			</div>
			<div class="col-md-7">
				<?php echo $site['Site']['maps']; ?>
			</div>
		</div>
	</div>
</div>