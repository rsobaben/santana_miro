<?php
$this->append('script');
    echo $this->Html->script(array('bootstrap-datetimepicker.min', 'bootstrap-datetimepicker.pt-BR')); ?> 
    <script type="text/javascript">
    jQuery(function($){
        $('.date').datetimepicker({
            language: 'pt-BR',
            pickTime: <?php echo isset($pickTime) ? ($pickTime ? 'true' : 'false') : 'false'; ?> 
        });
    });
    </script>
<?php
$this->end();
$this->append('css');
    echo $this->Html->css('bootstrap-datetimepicker.min');
$this->end();