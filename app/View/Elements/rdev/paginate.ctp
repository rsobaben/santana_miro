<?php if ($this->Paginator->counter("{:pages}") > 1) : ?>
	<div id="pagination">
		<ul class="pagination">
			<li>
			<?php
				echo $this->Paginator->prev(
					'«',
					array('tag' => false),
					null,
					array('class' => 'disabled', 'tag' => 'a')
				);
				?>
			</li>
		<?php 
			echo $this->Paginator->numbers(array(
                'separator'    => '',
                'modulus '     => 6,
                'before'       => '',
                'after'        => '',
                'tag'          => 'li',
                'currentClass' => 'pag-ativo',
                'currentTag'   => 'a'
			));
			?>
			<li>
			<?php 
				echo $this->Paginator->next(
					'»',
					array('tag' => false),
					null,
					array('class' => 'disabled', 'tag' => 'a')
				);
				?> 
			</li>
		</ul>
	</div>
<?php endif; ?>