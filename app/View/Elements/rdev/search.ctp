<?php $model = key($this->request->params['models']); ?>
<?php $contr = $this->request->params['controller']; ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <?php echo __('Search'); ?>
    </div>
<?php
    echo $this->Form->create($model, array(
        'url' => array(
            'action' => 'find',
            'rdev' => true,
            'controller' => $contr,
        ),
        'inputDefaults' => array(
            'class' => 'form-control',
            'div'   => array(
                'class' => 'form-group'
            )
        )
    )); ?>
    <div class="panel-body">
    <div class="input-group">
    <?php
        echo $this->Form->input('key',array(
            'label'       => false,
            'placeholder' => __('Search'),
            'class' => 'form-control',
            'value' => $key = $this->Session->read("{$model}.search.key"),
            'required'
        ));

        echo $this->Form->submit(__('Search'),array(
            'class' => 'btn btn-default',
            'div' => array('class' => 'input-group-btn'))
        );  
        ?>
    </div>
    <?php 
        if($key) {
            foreach (explode(' ', $key) as $k) {
                echo $this->html->link(
                    "{$k} <i class=\"glyphicon glyphicon-remove\"></i>",
                    array('action' => 'find', $k),
                    array('class' => 'label label-default margin-right', 'escape'=>false)
                );		
            }
        }

        ?>
    </div>
<?php echo $this->Form->end(); ?>
</div>