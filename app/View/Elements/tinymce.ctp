<?php
$this->append('script'); 
	echo $this->Html->script('tinymce/tinymce.min'); ?>
	<script type="text/javascript">
		jQuery(function($) {
			tinymce.init({
				selector: "textarea.editor",
				convert_urls: false,
				language: 'pt_BR',
				plugins: [
					"link image",
					"code",
				],
				toolbar: "undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
			});
			$('.editor').parents('form').find('[type="submit"]').click(function(){
				tinyMCE.triggerSave();
			})
	});
	</script>
<?php
$this->end();