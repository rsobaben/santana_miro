<?php $url = $this->Html->url(array('controller' => 'users', 'action' => 'redefine', $data['token'], 'rdev'=>false), true); ?>
<div style="width:650px;margin:0 auto; border:1px solid #DDD">
    <div style="background:#E8E8E8;color:#333;">
        Recuperação de Senha
    </div>
    <div style="padding:10px 5px; background:#FFF;color:#444;">
        <p>
            O processo de recuperação de senha foi iniciado em nosso sistema, 
            existe um usuário &lt;<?php echo $data['name']; ?>&gt; asssociado a este email
            &lt;<?php echo $data['email']; ?>&gt;.
        </p>
        <p>
            Clique aqui para recuperar sua senha: 
            <?php echo $this->Html->link($url, $url, array('escape' => false)); ?>
        </p>
    </div>
    <div style="background:#E8E8E8;color:#333; text-align:right;">
        <?php echo date('d/m/Y H:i'); ?>
    </div>
</div>