<?php $this->append('css'); ?>
	<style type="text/css">
		.e-container{width:600px;position:absolute;left:50%;top:50%;padding:20px;margin:-200px 0 0 -322px;border:2px solid #EEE;background:#FFF}
		.e-container h1{text-align:center}
		.e-container li{list-style-position:inside}
		/* css3 */
		.round{border-radius:4px;-moz-border-radius:4px;-webkit-border-radius:4px}
	</style>
<?php $this->end(); ?>

	<div class="e-container round">
		<h1>Página não encontrada!</h1>
		<div>
			<p>A página que procura parece não existir.</p>
			<p>Você deve ter chegado aqui porque:</p>
			<ul>
				<li>Digitou um endereço incorreto</li>
				<li>Clicou em um link que não mais existe</li>
				<li>Tentou adivinhar algo e não conseguiu</li>
			</ul>
		</div>		
		<script type="text/javascript">
		// <![CDATA[
		var GOOG_FIXURL_LANG = (navigator.language || "").slice(0,2),
			GOOG_FIXURL_SITE = location.host;
		// ]]>
		</script>
		<script src="http://linkhelp.clients.google.com/tbproxy/lh/wm/fixurl.js" type="text/javascript"></script>
	</div>
