<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
	<title><?php echo $title_for_layout; ?></title>
</head>
<body>
    <table style="width:700px;border-spacing:0;border-collapse:collpase;border:1px solid #DDD;">
        <tr>
            <th style="background: #EEE;text-align: right;padding:10px; color:#333;">
                Santana e Mirô
            </th>
        </tr>
        <tr>
            <th style="text-align:center;padding:20px 0">
            <?php 
                echo $this->Html->link(
                    $this->Html->image('marca.png', array('fullBase' => true)),
                    Router::url('/', true),
                    array('escape' => false)
                ); ?>
            </th>
        </tr>
        <tr>
        	<td style="padding:10px 50px;">
                <?php echo $this->fetch('content'); ?>
            </td>
        </tr>
        <tr>
            <td style="background: #EEE;color:#333;text-align:center;padding:20px 10px">
                Santana e Mirô &copy; <?php echo date('Y'); ?>
            </td>
        </tr>
    </table>
</body>
</html>