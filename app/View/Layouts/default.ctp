<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $this->fetch('title'); ?></title>
        <?php echo $this->fetch('meta'); ?> 
		<meta name="author" content="rdev 1.0: Rafael Benites">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php echo $this->Html->css(array('bootstrap.min', 'style')); ?>
        <?php echo $this->fetch('css'); ?> 
        <?php echo $this->Html->script(array('jquery', 'modernizr-2.6.2.min')); ?>
    </head>
    <body>
        <header>
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-md-offset-4">
						<nav role="navigation">
							<ul class="nav nav-pills">
								<li>
									<?php echo $this->Html->link(__('Home'), '/', array()); ?>
								</li>
								<li> 
									<?php echo $this->Html->link(__('About'), '/about', array()); ?>
								</li>
								<li> 
									<?php echo $this->Html->link(__('Products'), '/products', array()); ?>
								</li>
								<li> 
									<?php echo $this->Html->link(__('Contact'), '/contact', array()); ?>
								</li>
							</ul>
						</nav>
					</div>
					<div class="col-md-4">
						<?php echo $this->Form->create('Product', array('action' => 'index', 'class' => 'form-inline')); ?>
							<div class="form-group">
								<?php echo $this->Form->button('<span class="glyphicon glyphicon-search"></span>'); ?>
							</div>
							<?php echo $this->Form->input('key', array('label' => false, 'div' => array('class' => 'form-group'), 'class' => 'form-control')); ?>
						<?php echo $this->Form->end(); ?>
					</div>
				</div>
			</div>
        </header>
		<article class="container" role="main">
			<?php echo $this->fetch('content'); ?>
		</article>
        <footer class="border-top">
			<?php echo $this->fetch('footer'); ?>
            <div class="container">
				<div class="row">
					<div class="col-md-5">
						<p>Todos os direitos reservados. 2014 Distribuidora Santana &amp; Mirô</p>
					</div>
					<div class="col-md-3">
						<?php echo $this->Html->link('Designer Alana Montagna', 'http://www.alanamontagna.com', array('class' => 'external')); ?>
					</div>
					<div class="col-md-4 text-right social-links">
						<?php echo $this->Html->link($this->Html->image('f.png'), $site['Link']['facebook'], array('escape' => false)); ?>
						<?php echo $this->Html->link($this->Html->image('g.png'), $site['Link']['gplus'], array('escape' => false)); ?>
					</div>
				</div>
            </div>
        </footer>

	<?php echo $this->Html->script(array('bootstrap.min', 'jquery-migrate-1.2.1.min', 'jquery.fancybox-1.3.4.pack', 'jquery.form')); ?>
	<script type="text/javascript">
	// <![CDATA[
		jQuery(function($){
			$(".external").attr({target:"_blank"});
			$("a[href='#']").css({cursor:'default'});
		});
	// ]]>
    </script>
    <?php echo $this->fetch('script'); ?>
</body>
</html>