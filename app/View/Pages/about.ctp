<?php echo $this->element('box'); ?>
<h1 class="page-title"><?php echo $page['Page']['title']; ?></h1>
<?php echo $page['Page']['text']; ?>

	<?php $i =0;
		foreach ($page['Media'] as $item) : ?>
		<?php if (!($i % 5)) : ?>
			<?php if ($i) : ?>
					<div style="clear:both"></div>
				</div>  <!-- /.box-products -->
			<?php endif; ?>
				<div class="box-products">
		<?php endif; ?>
					<div class="product">
					<?php 
						echo $this->Html->link(
							$this->Media->image($item, 190, 106),
							Router::url($item['file'], true),
							array('escape' => false, 'class' => 'fancybox', 'rel' => 'galeria')
						); ?>
					</div> <!-- /.product -->
	<?php
		$i++;
		endforeach; ?>
					<div style="clear:both"></div>
				</div> <!-- /.box-products -->

<?php $this->append('css'); ?>
	<style type="text/css">
		body {margin-bottom: 260px;}
		footer{height:260px;}
	</style>
<?php echo $this->Html->css('fancybox/jquery.fancybox-1.3.4'); ?>
<?php $this->end();
$this->append('footer'); 
	echo $this->element('footer');
$this->end();
$this->append('script'); ?>
<script type="text/javascript">
jQuery(function(){
	$('.fancybox').fancybox();
})
</script>
<?php $this->end();
// title
$this->assign('title', $page['Page']['title'] .' | ' .  $site['Site']['title']);