<?php echo $this->element('cat_menu'); ?>
<?php echo $this->element('box'); ?>
<div class="row contact-page">
	<div class="col-md-6 ">
		<h1 class="page-title"><?php echo $page['Page']['title']; ?></h1>
		<?php echo $page['Page']['text']; ?>
		<?php echo $this->Session->flash(); ?>
		<?php echo $this->Form->create('Contact', array('inputDefaults' => array('label' => false, 'class' => 'form-control', 'div' => array('tag' => 'p')))); ?>
		<?php echo $this->Form->input('name', array('placeholder' => __('Name'))); ?>
		<?php echo $this->Form->input('email', array('placeholder' => __('E-mail'))); ?>
		<?php echo $this->Form->input('message', array('placeholder' => __('Message'), 'type' => 'textarea')); ?>

		<?php echo $this->Form->submit(__('Send'), array('class' => 'btn btn-success')); ?>

		<?php echo $this->Form->end(); ?>
	
	</div>
	<div class="col-md-6">
		<h2 class="page-subtitle contact-subtitle">Endereço:</h2>
		<p class="address">
			<i class="glyphicon glyphicon-map-marker"></i> <?php echo $site['Site']['address']; ?><br>
			CEP: <?php echo $site['Site']['zipcode']; ?> | <?php echo $site['Site']['city_state']; ?>
		</p>
		<p> <i class="glyphicon glyphicon-earphone"></i> <?php echo $site['Site']['phone']; ?></p>
		<p> <i class="glyphicon glyphicon-envelope"></i> <?php echo $site['Site']['email']; ?></p>
		<?php echo $site['Site']['maps']; ?>
	</div>
</div>
<?php
// title
$this->assign('title', $page['Page']['title'] .' | ' .  $site['Site']['title']);