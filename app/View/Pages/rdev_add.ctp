<div class="panel panel-default">	
	<div class="panel-heading">
        <?php echo __('Add Page'); ?> 
        <div class="pull-right text-right"> 
			<?php echo $this->Html->link(__('%s List', '<i class="glyphicon glyphicon-th-list"></i>'), array('action' => 'find'), array('escape' => false, 'class' => 'btn btn-xs btn-default')); ?>
        </div>        
    </div>
	<?php
		echo $this->Form->create('Page',array(
        'inputDefaults' => array(
            'class' => 'form-control',
            'div'   => array(
                'class' => 'form-group'
            ),
			'role' => 'form'
        )
    )); ?>
    <div class="panel-body">
	<?php
		echo $this->Form->input('has_text', array('before' => '<label>', 'after' => '</label>', 'type' => 'checkbox', 'class' => false, 'div' => array('class' => 'checkbox')));
		echo $this->Form->input('has_media', array('before' => '<label>', 'after' => '</label>', 'type' => 'checkbox', 'class' => false, 'div' => array('class' => 'checkbox')));
		echo $this->Form->input('title');
		echo $this->Form->input('text');
		echo $this->Form->input('description');
		echo $this->Form->input('keywords');
?>
   
	</div>
    <div class="panel-footer">
		<?php echo $this->Form->submit(__('Send'), array('class' => 'btn btn-primary', 'div' => array('class' => 'text-right'))); ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

