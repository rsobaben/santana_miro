<div class="panel panel-default">	
	<div class="panel-heading">
        <?php echo __('Edit Page'); ?> 
        <div class="pull-right text-right"> 
			<?php echo $this->Html->link(__('%s List', '<i class="glyphicon glyphicon-th-list"></i>'), array('action' => 'find'), array('escape' => false, 'class' => 'btn btn-xs btn-default')); ?>
			<?php echo $this->Form->postLink(__('%s Delete', '<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $this->request->data('Page.id')), array('escape' => false, 'class' => 'btn btn-xs btn-danger'), __('Are you sure you want to delete # %s?', $this->request->data('Page.id'))); ?>
        </div>        
    </div>
	<?php
		echo $this->Form->create('Page',array(
        'inputDefaults' => array(
            'class' => 'form-control',
            'div'   => array(
                'class' => 'form-group'
            )
        )
    )); ?>
    <div class="panel-body">
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('title');
		if ($this->request->data('Page.has_text')) : 
			echo $this->Form->input('text', array('class' => 'editor'));
			echo $this->element('tinymce');
		endif;
	//	echo $this->Form->input('description');
	//	echo $this->Form->input('keywords');
		if ($this->request->data('Page.has_media')) :
			echo $this->Media->iframe('Page', $this->request->data('Page.id'));
		endif;
?>
   
	</div>
    <div class="panel-footer">
		<?php echo $this->Form->submit(__('Send'), array('class' => 'btn btn-primary', 'div' => array('class' => 'text-right'))); ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

