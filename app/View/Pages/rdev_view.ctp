<div class="panel panel-default">	
	<div class="panel-heading">
        <?php echo __('Visualizar Page'); ?> 
        <div class="pull-right text-right">
			<?php echo $this->Html->link(__('%s List', '<i class="glyphicon glyphicon-th-list"></i>'), array('action' => 'find'), array('escape' => false, 'class' => 'btn btn-xs btn-default')); ?>
			<?php echo $this->Html->link(__('%s Edit', '<i class="glyphicon glyphicon-edit"></i>'), array('action' => 'edit', $page['Page']['id']), array('escape' => false, 'class' => 'btn btn-xs btn-warning')); ?>
			<?php echo $this->Form->postLink(__('%s Delete', '<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $page['Page']['id']), array('escape' => false, 'class' => 'btn btn-xs btn-danger'), __('Are you sure you want to delete # %s?', $page['Page']['id'])); ?>
        </div>
    </div>
    
    <table class="table table-striped" cellpadding="0" cellspacing="0">
	<tr>
		<th scope="row"><?php echo __('Id'); ?></th>
		<td><?php echo h($page['Page']['id']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Title'); ?></th>
		<td><?php echo h($page['Page']['title']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Text'); ?></th>
		<td><?php echo ($page['Page']['text']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Description'); ?></th>
		<td><?php echo h($page['Page']['description']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Keywords'); ?></th>
		<td><?php echo h($page['Page']['keywords']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Created'); ?></th>
		<td><?php echo h($page['Page']['created']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Modified'); ?></th>
		<td><?php echo h($page['Page']['modified']); ?>&nbsp;</td>
	</tr>
 
    </table>
    <div class="panel-footer text-right">
		<?php echo $this->Html->link(__('Cancel'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?>
    </div>
</div>
