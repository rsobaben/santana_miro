<?php echo $this->element('cat_menu'); ?>
<?php echo $this->element('box'); ?>
<h1 class="page-title"><?php echo $title; ?></h1>

<?php if (empty($itens)): ?>
	<p><?php echo __('No product found!'); ?></p>
<?php else : ?>
	<?php foreach ($itens as $i => $item) : ?>
		<?php if (!($i % 5)) : ?>
			<?php if ($i) : ?>
					<div style="clear:both"></div>
				</div>  <!-- /.box-products -->
			<?php endif; ?>
				<div class="box-products">
		<?php endif; ?>
					<div class="product">
					<?php 
						echo $this->Html->link(
							$this->Media->image(array_shift($item['Media']), 190, 106),
							array(
								'controller' => 'products', 
								'action'     => 'view',
								'slug'       => Inflector::slug(str_replace('<br>', '', $item['Category']['name']), '-'),
								'category'   => $item['Category']['id'],
								'product'    => Inflector::slug($item['Product']['name'], '-'),
								'id'         => $item['Product']['id']
							),
							array('escape' => false)
						); ?>
					</div> <!-- /.product -->
	<?php endforeach; ?>
					<div style="clear:both"></div>
				</div> <!-- /.box-products -->
	<?php echo $this->element('rdev/paginate'); ?>
<?php endif; ?>
<?php $this->append('css'); ?>
	<style type="text/css">
		body {margin-bottom: 260px;}
		footer{height:260px;}
	</style>
<?php $this->end();
$this->append('footer'); 
	echo $this->element('footer');
$this->end(); 

$this->assign('title', $title .' | ' .  $site['Site']['title']);