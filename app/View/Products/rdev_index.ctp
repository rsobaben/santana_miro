<?php echo $this->element('rdev/search'); ?>
<div class="panel panel-default">	
	<div class="panel-heading">
        <?php echo __('Products'); ?> 
        <div class="pull-right text-right">
			<?php echo $this->Html->link(__('%s List', '<i class="glyphicon glyphicon-th-list"></i>'), array('action' => 'find'), array('escape' => false, 'class' => 'btn btn-xs btn-default')); ?>
			<?php echo $this->Html->link(__('%s Add',  '<i class="glyphicon glyphicon-plus"></i>'), array('action' => 'add'), array('escape' => false, 'class' => 'btn btn-xs btn-success')); ?>
        </div>
    </div>
<?php if (!empty($products)) : ?>
	<table class="table table-striped" cellpadding="0" cellspacing="0">
        <tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('active'); ?></th>
			<th><?php echo $this->Paginator->sort('category_id'); ?></th>
            <th class="actions text-right"><?php echo __('Actions'); ?></th>
        </tr>
	<?php foreach ($products as $product): ?>
	<tr>
		<td><?php echo h($product['Product']['id']); ?>&nbsp;</td>
		<td><?php echo h($product['Product']['name']); ?>&nbsp;</td>
		<td><i class="glyphicon glyphicon-<?php echo $product['Product']['active'] ? 'ok':'remove'; ?>"></i>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($product['Category']['name'], array('controller' => 'categories', 'action' => 'view', $product['Category']['id'])); ?>
		</td>
		<td class="col-md-3 text-right">
			<?php echo $this->Html->link(__('%s View', '<i class="glyphicon glyphicon-file"></i>'), array('action' => 'view', $product['Product']['id']), array('escape' => false, 'class' => 'btn btn-xs btn-default')); ?>
			<?php echo $this->Html->link(__('%s Edit', '<i class="glyphicon glyphicon-edit"></i>'), array('action' => 'edit', $product['Product']['id']), array('escape' => false, 'class' => 'btn btn-xs btn-warning')); ?>
			<?php echo $this->Form->postLink(__('%s Delete', '<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $product['Product']['id']), array('escape' => false, 'class' => 'btn btn-xs btn-danger'), __('Are you sure you want to delete # %s?', $product['Product']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
<?php else : ?> 
    <br>
    <div class="alert alert-block alert-warning">
        <?php echo __('Nothing found!'); ?> 
    </div>
<?php endif; ?> 
    <div class="panel-footer text-center">
        <?php echo $this->element('rdev/paginate'); ?>&nbsp; 
    </div>
</div>
