<?php echo $this->element('cat_menu'); ?>
<?php echo $this->element('box'); ?>
<h1 class="page-title">
<?php 
	echo $this->Html->link(
		str_replace('<br>', '', $product['Category']['name']),
		array(
			'controller' => 'products',
			'action'     => 'index',
			'slug'       => Inflector::slug(str_replace('<br>', ' ', $product['Category']['name']), '-'),
			'category'   => $product['Category']['id'], 
		),
		array('escape' => false)
	); ?> > <?php echo $product['Product']['name']; ?>
</h1>

<div class="row">
	<div class="col-md-4">
		<div class="box-products">
		<?php 
			echo $this->Html->link(
				$this->Media->image($a = array_shift($product['Media']), 300, 168),
				Router::url($a['file'], true),
				array('escape' => false, 'class' =>  'fancybox')
			); ?>
		</div>
	</div>
	<div class="col-md-8 product-description">
		<p><?php echo $product['Product']['name']; ?></p>
		<p>
		<?php 
			echo $this->Html->link(
				$product['Brand']['name'],
				array('controller' => 'products', 'action' => 'index', 'brand' => $product['Brand']['id']),
				array('escape' => false)
			); ?>
		</p>
		<?php echo $product['Product']['description']; ?>
		<p><?php echo $product['Product']['code']; ?></p>
		<p><a href="javascript:history.back(-1)"><i class="glyphicon glyphicon-chevron-left"></i></a></p>
	</div>
</div>


<?php $this->append('css'); ?>
	<?php echo $this->Html->css('fancybox/jquery.fancybox-1.3.4'); ?>
	<style type="text/css">
		body {margin-bottom: 260px;}
		footer{height:260px;}
	</style>
<?php $this->end();
$this->append('script');
	echo $this->Html->script('fancybox/jquery.fancybox-1.3.4'); ?>
	<script type="text/javascript">
	jQuery(function($){
		$('.fancybox').fancybox();
	});
	</script>
<?php  $this->end();
$this->append('footer'); 
	echo $this->element('footer');
$this->end();

$this->assign('title', $product['Product']['name'] .' | ' .  $site['Site']['title']);