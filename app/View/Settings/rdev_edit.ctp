<div class="panel panel-default">	
	<div class="panel-heading">
        <?php echo __('Editar Setting'); ?> 
        <div class="pull-right text-right"> 
			<?php echo $this->Html->link(__('%s List', '<i class="glyphicon glyphicon-th-list"></i>'), array('action' => 'find'), array('escape' => false, 'class' => 'btn btn-xs btn-default')); ?>
			<?php echo $this->Form->postLink(__('%s Delete', '<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $this->request->data('Setting.id')), array('escape' => false, 'class' => 'btn btn-xs btn-danger'), __('Are you sure you want to delete # %s?', $this->request->data('Setting.id'))); ?>
        </div>        
    </div>
	<?php
		echo $this->Form->create('Setting',array(
        'inputDefaults' => array(
            'class' => 'form-control',
            'div'   => array(
                'class' => 'form-group'
            )
        )
    )); ?>
    <div class="panel-body">
	<?php
		echo $this->Form->input('id');

		echo $this->Form->input('label');
		echo $this->Form->input('value');
?>
   
	</div>
    <div class="panel-footer">
		<?php echo $this->Form->submit(__('Send'), array('class' => 'btn btn-primary', 'div' => array('class' => 'text-right'))); ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

