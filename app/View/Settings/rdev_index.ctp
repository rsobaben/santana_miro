<?php echo $this->element('rdev/search'); ?>
<div class="panel panel-default">	
	<div class="panel-heading">
        <?php echo __('Settings'); ?>        
        <div class="pull-right text-right">
        	<?php echo $this->Html->link(__('%s List', '<i class="glyphicon glyphicon-th-list"></i>'), array('action' => 'find'), array('escape' => false, 'class' => 'btn btn-xs btn-default')); ?>
        </div>
    </div>
<?php if (!empty($Settings)) : ?>
	<table class="table table-striped" cellpadding="0" cellspacing="0">
	<?php
        $group = null;
        foreach ($Settings as $configuraco) : ?>
        <?php if ($group != $configuraco['Setting']['setting']) : ?>
            <tr>
                <th colspan="5">
                    <?php echo $group = $configuraco['Setting']['setting']; ?>
                </th>
            </tr>
            <tr>
                <th><?php echo ('id'); ?></th>
                <th><?php echo ('label'); ?></th>
                <th class="actions text-right"><?php echo __('Actions'); ?></th>
            </tr>
        <?php endif; ?>
        <tr>
            <td><?php echo h($configuraco['Setting']['id']); ?>&nbsp;</td>
            <td><?php echo h($configuraco['Setting']['label']); ?>&nbsp;</td>
            <td class="col-md-3 text-right">
                <?php echo $this->Html->link(__('%s View', '<i class="glyphicon glyphicon-file"></i>'), array('action' => 'view', $configuraco['Setting']['id']), array('escape' => false, 'class' => 'btn btn-xs btn-default')); ?>
                <?php echo $this->Html->link(__('%s Edit', '<i class="glyphicon glyphicon-edit"></i>'), array('action' => 'edit', $configuraco['Setting']['id']), array('escape' => false, 'class' => 'btn btn-xs btn-warning')); ?>
            </td>
        </tr>
    <?php endforeach; ?>
	</table>
<?php else : ?>
    <br>
    <div class="alert alert-block alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <?php echo __('Nothing Found!'); ?>
    </div>
<?php endif; ?>
    <div class="panel-footer text-center">
        <?php echo $this->element('rdev/paginate'); ?>&nbsp;
    </div>
</div>