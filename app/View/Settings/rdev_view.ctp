<div class="panel panel-default">	
	<div class="panel-heading">
        <?php echo __('Visualizar Setting'); ?> 
        <div class="pull-right text-right">
			<?php echo $this->Html->link(__('%s List', '<i class="glyphicon glyphicon-th-list"></i>'), array('action' => 'find'), array('escape' => false, 'class' => 'btn btn-xs btn-default')); ?>
			<?php echo $this->Html->link(__('%s Edit', '<i class="glyphicon glyphicon-edit"></i>'), array('action' => 'edit', $setting['Setting']['id']), array('escape' => false, 'class' => 'btn btn-xs btn-warning')); ?>
			<?php echo $this->Form->postLink(__('%s Delete', '<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $setting['Setting']['id']), array('escape' => false, 'class' => 'btn btn-xs btn-danger'), __('Are you sure you want to delete # %s?', $setting['Setting']['id'])); ?>
        </div>
    </div>
    
    <table class="table table-striped" cellpadding="0" cellspacing="0">
	<tr>
		<th scope="row"><?php echo __('Id'); ?></th>
		<td><?php echo h($setting['Setting']['id']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Setting'); ?></th>
		<td><?php echo h($setting['Setting']['setting']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Field'); ?></th>
		<td><?php echo h($setting['Setting']['field']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Label'); ?></th>
		<td><?php echo h($setting['Setting']['label']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Value'); ?></th>
		<td><?php echo h($setting['Setting']['value']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Visible'); ?></th>
		<td><?php echo h($setting['Setting']['visible']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Created'); ?></th>
		<td><?php echo h($setting['Setting']['created']); ?>&nbsp;</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Modified'); ?></th>
		<td><?php echo h($setting['Setting']['modified']); ?>&nbsp;</td>
	</tr>
 
    </table>
    <div class="panel-footer text-right">
		<?php echo $this->Html->link(__('Cancel'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?>
    </div>
</div>
