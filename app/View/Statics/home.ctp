<?php echo $this->element('cat_menu'); ?>

<?php $this->append('css'); ?>
<style type="text/css">
.logo{position:absolute; top:50px; left:50px;}
.rel{position:relative;padding-top:20px}
.carousel-indicators{bottom:-50px}
	.carousel-indicators li{background: #54933f;border-color: #54933f}
	.carousel-indicators .active{background: #bccd49;border-color: #bccd49;}
.carousel-control{width:40px; height:40px; background: #54933f;background-image : none;top:163px;}
.carousel-control.left{right:30px;left:auto;}
.carousel-control.right{right:-10px}
.shadow{margin-bottom:40px}
.products-call{height:200px;line-height:200px;}
.products-call h3{display:inline; vertical-align: middle;}
.down{position: absolute; bottom:-5px;right:0}
.player {position: relative;}
.player .carousel-control{top:75px;}
.player .left{right:45px;left:auto;}
.player .right{right:5px}
.carousel {margin:0;}
.carousel li img{margin-right: 15px;
-webkit-border-radius: 10px;
   -moz-border-radius: 10px;
        border-radius: 10px;
}
</style>
<?php $this->end(); ?>


<div class="container rel">
	<div id="carousel-example-generic" class="row carousel slide" data-ride="carousel">
	  <!-- Indicators -->
		<ol class="carousel-indicators">
		<?php foreach ($banners as $i => $banner): ?>
			<li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php echo $i ? '' : 'active'; ?>"></li>
		<?php endforeach; ?>
		</ol>

	  <!-- Wrapper for slides -->
	  <div class="carousel-inner" role="listbox">
		<?php foreach ($banners as $i => $banner): ?>
			<div class="item <?php echo $i ? '' : 'active'; ?>">
			<?php if (!empty($banner['Banner']['url'])): ?>
				<a href="<?php echo $banner['Banner']['url']; ?>" class="external">
			<?php endif; ?>
					<?php echo $this->Media->image(array_shift($banner['Media']), 1140, 343); ?>
			<?php if (!empty($banner['Banner']['url'])): ?>
				</a>
			<?php endif; ?>
				<div class="carousel-caption hide"><?php echo $banner['Banner']['name']; ?></div>
			</div>
		<?php  endforeach; ?>
	  </div>

	  <!-- Controls -->
	  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	  </a>
	  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
		<?php echo $this->Html->image('border.png', array('class' => 'down')); ?>
	  </a>
	</div>
	<?php echo $this->Html->image('marca.png', array('class' => 'logo')); ?>
	<div class="shadow"></div>
</div>

<div class="row">
	<div class="col-md-10 player">
		<div class="box-products">
			<div class="row carousel">
				<ul>
				<?php foreach ($products as $item): ?>
					<li>
					<?php 
						echo $this->Html->link(
							$this->Media->image(array_shift($item['Media']), 270, 135),
							array(
								'controller' => 'products', 
								'action'     => 'view',
								'slug'       => Inflector::slug(str_replace('<br>', '', $item['Category']['name']), '-'),
								'category'   => $item['Category']['id'],
								'product'    => Inflector::slug($item['Product']['name'], '-'),
								'id'         => $item['Product']['id']
							),
							array('escape' => false)
						); ?>
					</li>
				<?php endforeach; ?>
				</ul>
			</div>
			<a class="left carousel-control">
				<span class="glyphicon glyphicon-chevron-left"></span>
			</a>
			<a class="right carousel-control">
				<span class="glyphicon glyphicon-chevron-right" ></span>
				<?php echo $this->Html->image('border.png', array('class' => 'down')); ?>
			</a>
		</div>
	</div>
	<div class="col-md-2 products-call text-center">
		<h3>Produtos</h3>
	</div>
</div>
<div class="row home-call">
	<div class="col-md-6 text-center">
		<h3>Faça seu Pedido</h3>
		<p>
			<?php echo $site['Site']['phone']; ?> <br>
			<?php echo $site['Site']['email']; ?>
		</p>
	</div>
</div>

<?php $this->append('css'); ?>
	<style type="text/css">
		body {margin-bottom: 260px;}
		footer{height:260px;}
	</style>
<?php $this->end();
$this->append('footer'); 
	echo $this->element('footer');
$this->end(); ?>
<?php $this->append('script'); ?>
	<?php echo $this->Html->script('jquery.jcarousellite.min'); ?>
	<script type="text/javascript">
	jQuery(function($) {
		$(".box-products .carousel").jCarouselLite({
			btnNext: ".box-products .right",
			btnPrev: ".box-products .left",
			visible:3
		});
	});
	</script>
<?php $this->end(); 

$this->assign('title', $site['Site']['title']);