<div class="col-md-4 col-md-offset-4">
    <?php echo $this->Session->flash(); ?>
    <?php echo $this->Session->flash('auth'); ?>
    <div class="panel panel-default">	
        <div class="panel-heading">
            <?php echo __('Admin Panel'); ?>
        </div>
<?php
    echo $this->Form->create(
        'User',
        array(
            'inputDefaults' => array(
                'class' => 'form-control',
                'div'   => array(
                    'class' => 'form-group'
                )
            )
        )
    ); ?>
        <div class="panel-body">
            <p><?php echo __('Inform your e-mail/password'); ?></p>
            <?php echo $this->Form->input('email'); ?>
            <?php echo $this->Form->input('password', array('label' => array('text' => 'Password'))); ?>
            <div class="row">
                <div class="col-md-6">
                    [<?php echo $this->Html->link(__('Recover your password'), array('action' =>'recover')); ?>]
                </div>
                <div class="col-md-6 text-right">
                    <?php echo $this->Form->submit(__('Login'), array('class' => 'btn btn-default')); ?>
                </div>
            </div>
        </div>
    <?php echo $this->Form->end(); ?>
    </div>
</div>