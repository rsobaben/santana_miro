<!-- template for: Users::rdev_add() -->
<div class="panel panel-default">	
	<div class="panel-heading">
        <?php echo __('Add User'); ?> 
        <div class="pull-right text-right">
        		<?php echo $this->Html->link(__('%s List', '<i class="glyphicon glyphicon-th-list"></i>'), array('action' => 'find'), array('escape' => false, 'class' => 'btn btn-xs btn-default')); ?>
        </div>        
    </div>

	<?php
		echo $this->Form->create(
        'User',
        array(
            'inputDefaults' => array(
                'class' => 'form-control',
                'div'   => array(
                    'class' => 'form-group'
                )
            )
        )
    ); ?>
    <div class="panel-body">
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('email');
		echo $this->Form->input('password');
		echo $this->Form->input('active', array('before' => '<label>', 'after' => '</label>', 'type' => 'checkbox', 'class' => false, 'div' => array('class' => 'checkbox')));
?>
    
	</div>
    <div class="panel-footer">
        <?php echo $this->Form->submit(__('Send'), array('class' => 'btn btn-primary', 'div' => array('class' => 'text-right'))); ?>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

