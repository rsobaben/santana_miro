<!-- template for: Users::rdev_view() -->
<div class="panel panel-default">	
	<div class="panel-heading">
        <?php echo __('Visualizar Usuário'); ?> 
        <div class="pull-right text-right">
            <?php echo $this->Html->link(__('%s List', '<i class="glyphicon glyphicon-th-list"></i>'), array('action' => 'find'), array('escape' => false, 'class' => 'btn btn-xs btn-default')); ?>
            <?php echo $this->Html->link(__('%s Edit', '<i class="glyphicon glyphicon-edit"></i>'), array('action' => 'edit', $user['User']['id']), array('escape' => false, 'class' => 'btn btn-xs btn-warning')); ?>
            <?php echo $this->Form->postLink(__('%s Delete', '<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $user['User']['id']), array('escape' => false, 'class' => 'btn btn-xs btn-danger'), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
            <?php echo $this->Html->link(__('%s Add', '<i class="glyphicon glyphicon-plus"></i>'), array('action' => 'add'), array('escape' => false, 'class' => 'btn btn-xs btn-success')); ?>
        </div>
    </div>
    
    <table class="table table-striped" cellpadding="0" cellspacing="0">
	<tr>
		<th scope="row"><?php echo __('Id'); ?></th>
		<td>
			<?php echo h($user['User']['id']); ?>&nbsp;
		</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Name'); ?></th>
		<td>
			<?php echo h($user['User']['name']); ?>&nbsp;
		</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Email'); ?></th>
		<td>
			<?php echo h($user['User']['email']); ?>&nbsp;
		</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Password'); ?></th>
		<td>
			<?php echo h($user['User']['password']); ?>&nbsp;
		</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Token'); ?></th>
		<td>
			<?php echo h($user['User']['token']); ?>&nbsp;
		</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Active'); ?></th>
		<td>
			<?php echo h($user['User']['active']); ?>&nbsp;
		</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Created'); ?></th>
		<td>
			<?php echo h($user['User']['created']); ?>&nbsp;
		</td>
	</tr>

	<tr>
		<th scope="row"><?php echo __('Modified'); ?></th>
		<td>
			<?php echo h($user['User']['modified']); ?>&nbsp;
		</td>
	</tr>
 
    </table>
    <div class="panel-footer text-right">
		<?php echo $this->Html->link(__('Cancel'), array('action' => 'index'), array('class' => 'btn btn-primary')); ?>
    </div>
</div>
