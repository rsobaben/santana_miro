<div class="col-md-4 col-md-offset-4">
    <?php echo $this->Session->flash(); ?>
    <div class="panel panel-default">	
        <div class="panel-heading">
            <?php echo __('REcover your password'); ?>
        </div>
<?php
    echo $this->Form->create(
        'User',
        array(
            'inputDefaults' => array(
                'class' => 'form-control',
                'div'   => array(
                    'class' => 'form-group'
                )
            )
        )
    ); ?>
        <div class="panel-body">
            <p><?php echo __('Inform your e-mail'); ?></p>
            <?php echo $this->Form->input('email'); ?>
            <div class="row">
                <div class="col-md-6">
                    <?php echo $this->Html->link(__('&laquo; Login'), array('action' =>'login'), array('escape' => false)); ?>
                </div>
                <div class="col-md-6 text-right">
                    <?php echo $this->Form->submit(__('Send'), array('class' => 'btn btn-default', 'div' => false)); ?>
                </div>
            </div>
        </div>
    <?php echo $this->Form->end(); ?>
    </div>
</div>