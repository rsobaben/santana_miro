<div class="col-md-4 col-md-offset-4">
    <?php echo $this->Session->flash(); ?>
    <div class="panel panel-default">	
        <div class="panel-heading">
            <?php echo __('Redefine Password'); ?>
        </div>
<?php
    echo $this->Form->create(
        'User',
        array(
            'inputDefaults' => array(
                'class' => 'form-control',
                'div'   => array(
                    'class' => 'form-group'
                )
            )
        )
    ); ?>
        <div class="panel-body">
            <p><?php echo __('Redefine password for: %s', (string)$this->request->data('User.email')); ?></p>
            <?php echo $this->Form->input('password'); ?>
            <?php echo $this->Form->input('confirm', array('type' => 'password')); ?>
            <div class="text-right">
                <?php echo $this->Form->input('id'); ?>
                <?php echo $this->Form->submit(__('Login'), array('class' => 'btn btn-default')); ?>
            </div>
        </div>
    <?php echo $this->Form->end(); ?>
    </div>
</div>